<?php
/**
 * Copyright © kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\InfoBar\Block;

class InfoBar extends \Magento\Framework\View\Element\Template
{

    /**
     * InfoBar constructor.
     * @param \Kowal\InfoBar\Helper\Data $helperData
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Kowal\InfoBar\Helper\Data $helperData,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->helperData = $helperData;
    }

    /**
     * @return string
     */
    public function GetText()
    {
        $text = $this->helperData->getGeneralCfg("text");
        return $text;
    }
    public function GetText1()
    {
        $text = $this->helperData->getGeneralCfg("text1");
        return $text;
    }
    public function GetText2()
    {
        $text = $this->helperData->getGeneralCfg("text2");
        return $text;
    }
    public function GetText3()
    {
        $text = $this->helperData->getGeneralCfg("text3");
        return $text;
    }
    public function GetText4()
    {
        $text = $this->helperData->getGeneralCfg("text4");
        return $text;
    }

    public function isEnable()
    {
        return $this->helperData->getGeneralCfg("enable");
    }
    public function isSticky()
    {
        return $this->helperData->getGeneralCfg("sticky");
    }
    public function getTextColor()
    {
        return $this->helperData->getGeneralCfg("color_field_text");
    }
    public function getBackgroundColor()
    {
        return $this->helperData->getGeneralCfg("color_field");
    }
    public function getDelayTime()
    {
        return $this->helperData->getGeneralCfg("time");
    }
}

