# Mage2 Module Kowal InfoBar

    ``kowal/module-infobar``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Przekierowanie klienta B2B na stronę dla klientów b2b

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_InfoBar`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-infobar`
 - enable the module by running `php bin/magento module:enable Kowal_InfoBar`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - enable (info_bar/settings/enable)

 - text (info_bar/settings/text)

 - bgcolor (info_bar/settings/bgcolor)

 - fontcolor (info_bar/settings/fontcolor)

 - fontsize (info_bar/settings/fontsize)


## Specifications

 - Block
	- InfoBar > infobar.phtml


## Attributes



